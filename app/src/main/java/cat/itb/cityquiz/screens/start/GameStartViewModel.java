package cat.itb.cityquiz.screens.start;

import android.view.View;

import androidx.lifecycle.ViewModel;

import java.util.List;

import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;

public class GameStartViewModel extends ViewModel {

    // TODO: Implement the ViewModel

    int response;

    public void test(){
        GameLogic gamelogic = RepositoriesFactory.getGameLogic();
        Game game = gamelogic.createGame(Game.maxQuestions,Game.possibleAnswers);
        game = gamelogic.answerQuestions(game,1);

    }
}
