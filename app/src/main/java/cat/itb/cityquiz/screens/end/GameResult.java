package cat.itb.cityquiz.screens.end;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Game;

public class GameResult extends Fragment {

    private GameResultViewModel mViewModel;
    public Game game;
    int resultado;
    TextView textoResultado;

    public static GameResult newInstance() {
        return new GameResult();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.game_result_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(GameResultViewModel.class);
        // TODO: Use the ViewModel
        resultado = game.getScore();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.id_onPlayScreen).setOnClickListener(this::switchToStartScreen);
        textoResultado = view.findViewById(R.id.scoreView);
        textoResultado.setText(resultado+"");
    }

    private void switchToStartScreen(View view) {
        Navigation.findNavController(view).navigate(R.id.navegarPantallaDeInicio);

    }


}
