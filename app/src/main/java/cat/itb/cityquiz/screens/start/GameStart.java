package cat.itb.cityquiz.screens.start;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.screens.gaming.OnPlaying;
import cat.itb.cityquiz.screens.gaming.OnPlayingViewModel;

public class GameStart extends Fragment {

    private GameStartViewModel mViewModel;
    OnPlayingViewModel onPlayingViewModel;

    public static GameStart newInstance() {
        return new GameStart();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.game_start_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(GameStartViewModel.class);
        // TODO: Use the ViewModel
        onPlayingViewModel = ViewModelProviders.of(getActivity()).get(OnPlayingViewModel.class);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.btn_startQuiz).setOnClickListener(this::switchToPlayingScreen);
    }

    private void switchToPlayingScreen(View view) {
        Navigation.findNavController(view).navigate(R.id.navegarPantallaDeJuego);
        onPlayingViewModel.startQuiz();
    }
}
