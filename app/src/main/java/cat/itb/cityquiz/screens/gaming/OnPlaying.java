package cat.itb.cityquiz.screens.gaming;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.domain.Answer;
import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;

public class OnPlaying extends Fragment {

    EditText botonSeleccionado;
    public Game game;
    private OnPlayingViewModel mViewModel;
    City city;


    public static OnPlaying newInstance() {
        return new OnPlaying();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.on_playing_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(OnPlayingViewModel.class);
        // TODO: Use the ViewModel
        game = mViewModel.getGame();
        //display(game);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //view.findViewById(R.id.id_onPlayScreen).setOnClickListener(this::switchToResultScreen);

        View.OnClickListener handler = new View.OnClickListener() {
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.btn_opcio1:
                        game.questionAnswered(new City(city.getName(),city.getCountry()));


                        Toast.makeText(v.getContext(),game.getCurrentQuestion()+"",Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.btn_opcio2:
                        game.questionAnswered(new City(city.getName(),city.getCountry()));
                        break;
                    case R.id.btn_opcio3:
                        game.questionAnswered(new City(city.getName(),city.getCountry()));
                        break;
                    case R.id.btn_opcio4:
                        game.questionAnswered(new City(city.getName(),city.getCountry()));
                        break;
                    case R.id.btn_opcio5:
                        game.questionAnswered(new City(city.getName(),city.getCountry()));
                        break;
                    case R.id.btn_opcio6:
                        game.questionAnswered(new City(city.getName(),city.getCountry()));
                        break;
                    default:
                        System.out.println("Error");
                        break;
                }
            }
        };

    }

    private void switchToResultScreen(View view) {
        //Navigation.findNavController(view).navigate(R.id.navegarPantallaDeResultado);
    }

}
