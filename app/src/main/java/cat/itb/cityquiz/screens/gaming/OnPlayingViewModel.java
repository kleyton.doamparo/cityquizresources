package cat.itb.cityquiz.screens.gaming;

import androidx.lifecycle.ViewModel;

import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;

public class OnPlayingViewModel extends ViewModel {
    // TODO: Implement the ViewModel

    GameLogic gameLogic = RepositoriesFactory.getGameLogic();
    Game game;

    public void startQuiz(){
        game = gameLogic.createGame(Game.maxQuestions,Game.possibleAnswers);
    }

    public Game getGame(){
        return game;
    }


}
